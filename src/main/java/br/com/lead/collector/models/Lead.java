package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIgnoreProperties(value = {"data"}, allowGetters = true)
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 150, message = "O nome deve ter entre 5 a 150 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Não pode ser só espaço")
    private String nome;

    @Email(message = "O formato de email é invalido")
    @NotNull(message = "Email não pode ser nulo")
    private String email;

    //@Null(message = "O campo data não é preenchido pelo usuário")
    private LocalDate data;

    @NotNull(message = "O campo Tipo Lead não pode ser nulo")
    //@Enumerated(EnumType.STRING)
    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Lead(){}

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public String getNome() {
        return nome;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }
}
