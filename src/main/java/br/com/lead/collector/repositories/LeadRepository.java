package br.com.lead.collector.repositories;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

//    @Query("Select * from lead where lead.email == ?")
    Iterable<Lead> findAllByTipoLead(TipoLeadEnum tipoLeadEnum);

}
