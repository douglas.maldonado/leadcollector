package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setNome("Douglas");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("douglas.maldonado@email.com");

        produto = new Produto();
        produto.setDescricao("Café do bom");
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.50);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarRegistrarLead() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            return lead;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDelead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDelead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));

        //exempplo json resposta
//        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/leads")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(jsonDelead))
//                .andExpect(MockMvcResultMatchers.status().isCreated())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));
//
//        MvcResult mvcResult = resultActions.andReturn();
//        String jsonResposta = mvcResult.getResponse().getContentAsString();
//        LeadDTO leadDTO = mapper.readValue(jsonResposta, LeadDTO.class);


    }

    @Test
    public void testarExibirTodos() throws Exception{
        Mockito.when(leadService.buscarTodos()).then(leads -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            List<Lead> leadList = new ArrayList<>();
            leadList.add(lead);

            Iterable<Lead> leadsRetorno = leadList;
            return leadsRetorno;
        });


        ObjectMapper mapper = new ObjectMapper();
        String jsonDelead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDelead)).andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    //precisa incluir o parametro
    @Test
    public void testarExibirTodosPorParametro() throws Exception{
        Mockito.when(leadService.buscarTodosPorTipoLead(lead.getTipoLead())).then(leads -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            List<Lead> leadList = new ArrayList<>();
            leadList.add(lead);

            Iterable<Lead> leadsRetorno = leadList;
            return leadsRetorno;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDelead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDelead)).andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarDeletarLead() throws Exception{
        //verificar pq não chama o metodo deletarLead
        //Mockito.verify(leadService, Mockito.times(1)).deletarLead(Mockito.anyInt());

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/9"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

    }

    @Test
    public void testarDeletarLeadNaoEcontrado() throws Exception {
        Mockito.doThrow(new RuntimeException("O Lead não foi encontrado"))
                .when(leadService).deletarLead(Mockito.anyInt());

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/9"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
