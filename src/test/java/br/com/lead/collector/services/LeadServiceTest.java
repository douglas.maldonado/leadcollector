package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    private Lead lead;
    private Produto produto;
    private List<Produto> produtos;


    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Douglas");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("douglas.maldonado@email.com");

        produto = new Produto();
        produto.setDescricao("Café do bom");
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.50);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarTodos(){
        Iterable<Lead> leads = Arrays.asList(lead);

        Mockito.when(leadRepository.findAll()).thenReturn(leads);
        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads,  leadIterable);

    }

    @Test
    public void testarAtualizarLead(){
        Mockito.when(produtoService.buscarTodosPorId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Jeff");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("jeef@email.com");
        leadTeste.setId(1);
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadTeste);
        Mockito.when(leadRepository.existsById(leadTeste.getId())).thenReturn(true);

        Lead leadObjeto = leadService.atualizarLead(leadTeste.getId(),leadTeste);

        Assertions.assertSame(leadTeste, leadObjeto);
    }

    @Test
    public void testarDeletarPorId(){
        Lead leads = lead;

        Mockito.when(leadRepository.existsById(lead.getId())).thenReturn(true);

        leadService.deletarLead(lead.getId());
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarBuscarPorLead(){
        Iterable<Lead> leads = Arrays.asList(lead);

        Mockito.when(leadRepository.findAllByTipoLead(lead.getTipoLead())).thenReturn(leads);
        Iterable<Lead> leadIterable = leadService.buscarTodosPorTipoLead(lead.getTipoLead());

        Assertions.assertEquals(leads,  leadIterable);

    }

    @Test
    public void testarBuscaPorId(){
        Lead leads = lead;
        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(lead.getId())).thenReturn(leadOptional);
        Lead leadObjeto = leadService.buscarPorId(lead.getId());

        Assertions.assertSame(leads, leadObjeto);
    }


    @Test
    public void testarSalvarLead(){
        Mockito.when(produtoService.buscarTodosPorId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Jeff");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("jeef@email.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadTeste);

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));


    }

}
